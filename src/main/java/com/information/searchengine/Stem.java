package com.information.searchengine;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import org.tartarus.snowball.ext.EnglishStemmer;

public class Stem {
	
	public void Stemmer() throws IOException{
		
		EnglishStemmer english = new EnglishStemmer();
		File file1 = new File("C:/Users/harshul/Desktop/ABCD1.txt");
		PrintWriter write = new PrintWriter("C:/Users/harshul/Desktop/ABCD3.txt");
		Scanner scanner = new Scanner(file1);
		StringBuilder builder = new StringBuilder();
	    while (scanner.hasNextLine()) {
	        builder.append(scanner.nextLine());
	        builder.append(" ");//Additional empty space needs to be added
	    }
	    String words[] = builder.toString().split(" ");
	    //System.out.println(Arrays.toString(strings));
	    for(int i = 0; i < words.length; i++){
	        english.setCurrent(words[i]);
	        english.stem();
	        write.println(english.getCurrent());
	    }
	    scanner.close();
	    write.close();	
	}
	
	public static void main(String[] args) throws IOException{
		
		Stem tester;
		tester = new Stem();   
		try {
				tester.Stemmer();
			} catch (IOException e) {
				e.printStackTrace();
			}    
	}
}

